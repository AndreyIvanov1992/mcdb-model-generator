package ru.moex.clientdb.metadata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:applicationContext.xml")
public class McdbModelGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(McdbModelGeneratorApplication.class, args);
    }
}
