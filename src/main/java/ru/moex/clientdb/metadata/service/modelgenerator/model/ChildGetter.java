package ru.moex.clientdb.metadata.service.modelgenerator.model;

import lombok.Builder;
import lombok.Getter;
import ru.moex.clientdb.metadata.model.Property;

@Getter
@Builder
public class ChildGetter {

    private String baseNameMethod;

    private String className;

    private String propertyConst;
}
