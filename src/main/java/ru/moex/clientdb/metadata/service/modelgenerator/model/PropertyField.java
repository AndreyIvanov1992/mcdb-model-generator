package ru.moex.clientdb.metadata.service.modelgenerator.model;

import lombok.Builder;
import lombok.Getter;
import ru.moex.clientdb.metadata.model.Property;

@Getter
@Builder
public class PropertyField {

    Property property;

    String constName;

    boolean needSetter;

    String type;

    String getterMethodName;

    String setterMethodName;

    boolean allFieldsInCommonMethod;

    String parentClassName;

    String parentGetterMethod;

    String parentSetterMethod;
}
