package ru.moex.clientdb.metadata.service.modelgenerator;


import ru.moex.clientdb.metadata.service.modelgenerator.model.ObjectTypeClassModel;

import java.util.List;

public interface ModelGenerator {

    List<ObjectTypeClassModel> generateModels(GeneratorSettings generatorSettings);

    void generateClassFile(ObjectTypeClassModel model, GeneratorSettings generatorSettings);

    default void generateClassFiles(GeneratorSettings generatorSettings) {
        generateModels(generatorSettings).forEach(objectTypeClassModel -> generateClassFile(objectTypeClassModel, generatorSettings));
    }
}
