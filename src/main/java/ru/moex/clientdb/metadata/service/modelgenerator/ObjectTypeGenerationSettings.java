package ru.moex.clientdb.metadata.service.modelgenerator;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ObjectTypeGenerationSettings {

    private String basePackage;

    private boolean generateListsOfChild;
}
