package ru.moex.clientdb.metadata.service.modelgenerator.model;

import lombok.Builder;
import lombok.Getter;
import ru.moex.clientdb.metadata.model.ObjectType;

import java.util.List;

@Getter
@Builder
public class ObjectTypeClassModel {

    private String classPackage;

    private String parentClass;

    private String className;

    private ObjectType objectType;

    private List<PropertyField> properties;

    private List<ChildGetter> childGetters;

    private List<DictionaryConst> dictionaryConsts;

}
