package ru.moex.clientdb.metadata.service.modelgenerator;

import lombok.Builder;
import lombok.Getter;

import java.util.Map;
import java.util.Set;

@Builder
@Getter
public class GeneratorSettings {

    private String baseFolder;

    private ObjectTypeGenerationSettings defaultSettings;

    private Set<String> objectTypes;

    private Map<String, ObjectTypeGenerationSettings> objectTypesSettings;
}
