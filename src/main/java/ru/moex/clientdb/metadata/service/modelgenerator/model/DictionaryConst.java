package ru.moex.clientdb.metadata.service.modelgenerator.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class DictionaryConst {

    private String constName;

    private long value;

    private String methodName;
}
