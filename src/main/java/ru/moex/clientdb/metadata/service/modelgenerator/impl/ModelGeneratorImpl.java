package ru.moex.clientdb.metadata.service.modelgenerator.impl;

import com.google.common.base.CaseFormat;
import com.google.common.base.Function;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.moex.clientdb.metadata.dao.MetaDataSource;
import ru.moex.clientdb.metadata.model.*;
import ru.moex.clientdb.metadata.service.modelgenerator.ModelGenerator;
import ru.moex.clientdb.metadata.service.modelgenerator.GeneratorSettings;
import ru.moex.clientdb.metadata.service.modelgenerator.ObjectTypeGenerationSettings;
import ru.moex.clientdb.metadata.service.modelgenerator.model.ChildGetter;
import ru.moex.clientdb.metadata.service.modelgenerator.model.DictionaryConst;
import ru.moex.clientdb.metadata.service.modelgenerator.model.ObjectTypeClassModel;
import ru.moex.clientdb.metadata.service.modelgenerator.model.PropertyField;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ModelGeneratorImpl implements ModelGenerator {

    private final MetaDataSource metaDataSource;

    private Configuration cfg = new Configuration(Configuration.VERSION_2_3_30);

    private final static String ABSTRACT_DICTIONARY_MODEL = "AbstractDictionaryModel";

    private final static String ABSTRACT_VIEW_MODEL = "AbstractViewModel";

    private final static String ABSTRACT_INFORMATION_OBJECT_MODEL = "AbstractInformationObjectModel";

    private final static String MCDB_OBJECT_MODEL = "McdbObjectModel";

    private final static List<String> COMMON_DICTINARY_PROPERTIES = Collections.singletonList("name");

    private final static List<String> COMMON_INFORMATION_OBJECT_PROPERTIES = Arrays.asList("store_date", "last_edit_time", "employee_id");

    @PostConstruct
    public void init() {
        cfg.setClassForTemplateLoading(this.getClass(), "\\");
    }

    @Autowired
    public ModelGeneratorImpl(MetaDataSource metaDataSource) {
        this.metaDataSource = metaDataSource;
    }

    private String packageToPath(String packageName) {
        return packageName.replace(".", File.separator);
    }

    @Override
    public List<ObjectTypeClassModel> generateModels(GeneratorSettings generatorSettings) {
        return getItemsToGenerate(generatorSettings).stream().map((Function<ObjectType, ObjectTypeClassModel>) objectType -> {

            ObjectTypeGenerationSettings settings = generatorSettings.getObjectTypesSettings().get(objectType.getName());

            if (settings == null) {
                settings = generatorSettings.getDefaultSettings();
            }

            return createObjectTypeClassModel(objectType, settings);
        }).collect(Collectors.toList());
    }

    private Set<ObjectType> getItemsToGenerate(GeneratorSettings generatorSettings) {
        if (generatorSettings.getObjectTypes() != null && !generatorSettings.getObjectTypes().isEmpty()) {
            return generatorSettings.getObjectTypes().stream().map(metaDataSource::getObjectTypeByName).collect(Collectors.toSet());
        } else {
            return metaDataSource.getObjectTypes();
        }
    }

    private ObjectTypeClassModel createObjectTypeClassModel(ObjectType objectType, ObjectTypeGenerationSettings settings) {
        ObjectTypeClassModel.ObjectTypeClassModelBuilder builder = ObjectTypeClassModel.builder();

        builder.classPackage(settings.getBasePackage());
        builder.className(convertToCamelCase(objectType.getName()));
        String parentClass = calculateParentClass(objectType);
        builder.parentClass(parentClass);
        builder.objectType(objectType);
        builder.properties(objectType.
                getProperties().
                stream().
                filter(property -> !getFieldToSkip(parentClass).contains(property.getName()) && property.getDataType() != DataType.BINARY).
                map(this::createPropertyField).
                collect(Collectors.toList()));
        builder.childGetters(objectType.getReferringProperties().stream().map(this::createChildGetter).collect(Collectors.toList()));
        return builder.build();
    }

    private Set<String> getFieldToSkip(String parentClass) {

        Set<String> result = new HashSet<>();
        result.add("id");
        if (parentClass.equals(ABSTRACT_DICTIONARY_MODEL)) {
            result.addAll(COMMON_DICTINARY_PROPERTIES);
        }
        if (parentClass.equals(ABSTRACT_INFORMATION_OBJECT_MODEL)) {
            result.addAll(COMMON_INFORMATION_OBJECT_PROPERTIES);
        }
        return result;
    }

    private PropertyField createPropertyField(Property property) {
        PropertyField.PropertyFieldBuilder propertyFieldBuilder = PropertyField.builder();

        propertyFieldBuilder.property(property);
        propertyFieldBuilder.constName(property.getName().toUpperCase());
        propertyFieldBuilder.needSetter(true);//Для тестирования.

        String getterPrefix = property.getDataType() == DataType.BOOLEAN ? "is" : "get";

        String propertyName = property.getName();

        if (property.getDataType() == DataType.INT) {
            propertyFieldBuilder.type("Long");
            propertyFieldBuilder.parentGetterMethod("getLongValue");
            propertyFieldBuilder.parentSetterMethod("setProperty");
        }

        if (property.getDataType() == DataType.FLOAT) {
            propertyFieldBuilder.type("Double");
            propertyFieldBuilder.parentGetterMethod("getDoubleValue");
            propertyFieldBuilder.parentSetterMethod("setProperty");
        }

        if (property.getDataType() == DataType.STRING) {
            propertyFieldBuilder.type("String");
            propertyFieldBuilder.parentGetterMethod("getPropertyValue");
            propertyFieldBuilder.parentSetterMethod("setProperty");
        }

        if (property.getDataType() == DataType.DATE) {
            propertyFieldBuilder.type("Date");
            propertyFieldBuilder.parentGetterMethod("getDateValue");
            propertyFieldBuilder.parentSetterMethod("setProperty");
        }

        if (property.getDataType() == DataType.DATETIME) {
            propertyFieldBuilder.type("LocalDateTime");
            propertyFieldBuilder.parentGetterMethod("getDateTimeValue");
            propertyFieldBuilder.parentSetterMethod("setProperty");
        }

        if (property.getDataType() == DataType.BOOLEAN) {
            propertyFieldBuilder.type("Boolean");
            propertyFieldBuilder.parentGetterMethod("getBooleanValue");
            propertyFieldBuilder.parentSetterMethod("setProperty");
        }

        if (property.getDataType() == DataType.REFERENCE) {
            propertyFieldBuilder.type(convertToCamelCase(property.getName()));
            propertyFieldBuilder.parentGetterMethod("getParentModel");
            propertyFieldBuilder.parentSetterMethod("setParentModel");
        }

        //objectType,
        List<String> reservedProperties = Arrays.asList(
                "informationObject",
                "stringId",
                "scheduleRemoval",
                "modelPool",
                "new",
                "objectTypeName",
                "objectType",

                "dateTimeValue",
                "dateValue",
                "longValue",
                "booleanValue",
                "propertyValue",
                "doubleValue",
                "parentModel",
                "reference",
                "childModels");
        if (reservedProperties.contains(propertyName)) {
            propertyName = propertyName + "_propertyValue";
        }

        propertyFieldBuilder.getterMethodName(getterPrefix + convertToCamelCase(propertyName));
        propertyFieldBuilder.setterMethodName("set" + convertToCamelCase(propertyName));

        if (property.getDataType() == DataType.REFERENCE) {
            propertyFieldBuilder.allFieldsInCommonMethod(property.getObjectType().getObjectKind() == ObjectKind.DICTIONARY);
            propertyFieldBuilder.parentClassName(convertToCamelCase(property.getObjectType().getName()));
        }

        return propertyFieldBuilder.build();
    }

    private DictionaryConst creteDictionaryConstConst(DictionaryValue dictionaryValue) {
        //TODO
        return null;
    }

    private ChildGetter createChildGetter(Property referencedProperty) {
        ChildGetter.ChildGetterBuilder builder = ChildGetter.builder();
        String childClassName = convertToCamelCase(referencedProperty.getObjectType().getName());

        builder.className(childClassName);

        builder.propertyConst(referencedProperty.getName().toUpperCase());
        boolean anotherReferenceToSameType = referencedProperty.
                getObjectType().
                getProperties().
                stream().
                anyMatch(property -> property != referencedProperty && property.getObjectType() == referencedProperty.getObjectType());

        String baseNameMethod = "get" + childClassName + "s";
        if (anotherReferenceToSameType) {
            baseNameMethod += "_as" + convertToCamelCase(referencedProperty.getName());
        }
        builder.baseNameMethod(baseNameMethod);
        return builder.build();
    }

    private String calculateParentClass(ObjectType objectType) {
        if (objectType.getObjectKind() == ObjectKind.DICTIONARY) {
            return ModelGeneratorImpl.ABSTRACT_DICTIONARY_MODEL;
        }

        if (objectType.getObjectKind() == ObjectKind.VIEW) {
            return ModelGeneratorImpl.ABSTRACT_VIEW_MODEL;
        }

        if (objectType.getObjectKind() == ObjectKind.INFORMATION_OBJECT) {
            List<String> propertyNames = objectType.getProperties().stream().map(Property::getName).collect(Collectors.toList());
            if (propertyNames.containsAll(COMMON_INFORMATION_OBJECT_PROPERTIES)) {
                return ModelGeneratorImpl.ABSTRACT_INFORMATION_OBJECT_MODEL;
            } else {
                return ModelGeneratorImpl.MCDB_OBJECT_MODEL;
            }
        }
        return ModelGeneratorImpl.MCDB_OBJECT_MODEL;
    }

    private String convertToCamelCase(String sourceWithUnderwriter) {
        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, sourceWithUnderwriter);
    }

    private String convertToCamelCaseFirstLower(String sourceWithUnderwriter) {
        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, sourceWithUnderwriter);
    }

    @Override
    public void generateClassFile(ObjectTypeClassModel model, GeneratorSettings generatorSettings) {
        try {
            Template template = cfg.getTemplate("mcdbModel.ftl");
            File file = new File(
                    generatorSettings.getBaseFolder() +
                            packageToPath(model.getClassPackage()) + File.separator +
                            model.getClassName()
                            + ".java");
            file.getParentFile().mkdirs();
            Writer fileWriter = new FileWriter(file);
            template.process(model, fileWriter);
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
