package ru.moex.clientdb.metadata.model;

import lombok.AllArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
public enum DataType {

    INT(1),
    FLOAT(2),
    STRING(3),
    DATE(4),
    DATETIME(5),
    BOOLEAN(6),
    BINARY(7),
    REFERENCE(8);

    @Setter
    private int id;

    public static DataType getById(int id){
        for(DataType objectKind : DataType.values()){
            if(id == objectKind.id) return objectKind;
        }
        return null;
    }
}
