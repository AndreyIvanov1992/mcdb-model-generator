package ru.moex.clientdb.metadata.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DictionaryValue {

    private long id;

    private String value;
}
