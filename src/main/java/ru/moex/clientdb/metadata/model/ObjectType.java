package ru.moex.clientdb.metadata.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class ObjectType {

    private int id;

    private String name;

    private String description;

    private ObjectKind objectKind;

    private String note;

    private String oldName;

    private Set<Property> properties;

    private Set<Property> referringProperties;

    public Property getProperty(String name) {
        if(StringUtils.isEmpty(name)) {
            throw new IllegalArgumentException("name can't be empty");
        }
        return properties.stream().filter(property -> name.equals(property.getName())).findAny().orElse(null);
    }

    private List<DictionaryValue> dictionaryValues;
}
