package ru.moex.clientdb.metadata.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class Property {

    private int id;

    private ObjectType objectType;

    private String name;

    private String description;

    private int propertySize;

    private String propertyGroup;

    private DataType dataType;

    private String note;

    private ObjectType referenceObjectType;

    private Property referenceProperty;

    private boolean required;

    private String valueFunction;

    private boolean controlled;

    private String oldName;

    private boolean UsingView;
}
