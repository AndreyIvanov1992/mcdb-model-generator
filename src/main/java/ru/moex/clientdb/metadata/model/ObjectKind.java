package ru.moex.clientdb.metadata.model;

import lombok.AllArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
public enum ObjectKind {

    DICTIONARY(3),
    INFORMATION_OBJECT(2),
    VIEW(4);

    @Setter
    private int id;

    public static ObjectKind getById(int id){
        for(ObjectKind dataType : ObjectKind.values()){
            if(id == dataType.id) return dataType;
        }
        return null;
    }
}
