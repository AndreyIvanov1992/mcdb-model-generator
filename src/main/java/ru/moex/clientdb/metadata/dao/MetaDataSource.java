package ru.moex.clientdb.metadata.dao;

import ru.moex.clientdb.metadata.model.ObjectType;

import java.util.Set;

/**
 * Источник метаданных объектов ЕКБД
 */
public interface MetaDataSource {

    /**
     * Получение типа объекта по названию
     *
     * @param name Название типа объекта
     * @return Тип объекта
     */
    ObjectType getObjectTypeByName(String name);

    /**
     * Получение типа объекта по id
     *
     * @param id ID типа объекта
     * @return Тип объекта
     */
    ObjectType getObjectTypeById(int id);

    /**
     * Получение множетсва всех объектов ЕКБД
     *
     * @return Множетсво всех объектов ЕКБД
     */
    Set<ObjectType> getObjectTypes();
}
