package ru.moex.clientdb.metadata.dao.impl;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.FactoryBean;
import ru.moex.clientdb.metadata.dao.MetaDataSource;
import ru.moex.clientdb.metadata.model.Property;
import ru.moex.clientdb.metadata.model.ObjectKind;
import ru.moex.clientdb.metadata.model.DataType;
import ru.moex.clientdb.metadata.model.ObjectType;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Фабрика бинов, особнанного на excel, для источника метаданных объектов ЕКБД в памяти
 */
@Slf4j
public class ExcelMetaDataSourceFactory implements FactoryBean<MetaDataSource> {

    @Setter
    private String objectTypeFileName;

    @Setter
    private String propertyFileName;

    @Override
    public MetaDataSource getObject() throws Exception {
        try {
            Set<ObjectType> objectTypeSet = createObjectTypeSet();
            feedProperties(objectTypeSet);
            return new MemoryMetaDataSource(objectTypeSet);
        } catch (Exception e) {
            log.error("Ошибка инициализации данных из экселя.", e);
            throw new Exception(e);
        }
    }

    @Override
    public Class<?> getObjectType() {
        return MetaDataSource.class;
    }

    private Set<ObjectType> createObjectTypeSet() {
        Set<ObjectType> objectTypeSet = new HashSet<>();
        XSSFWorkbook objectTypeWb;
        try {
            //          File objectTypeFile = new File(objectTypeFileName);
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(objectTypeFileName);

            if (inputStream == null) {
                throw new RuntimeException("Не удалось получить файл " + objectTypeFileName + " с типами объектов.");
            }
            objectTypeWb = new XSSFWorkbook(inputStream);
            Sheet sheet = objectTypeWb.getSheetAt(0);
            for (Row row : sheet) {
                if (row.getRowNum() != 0) {
                    objectTypeSet.add(createObjectType(row));
                }
            }
            return objectTypeSet;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ObjectType createObjectType(Row row) {
        int id = getIntValue(row.getCell(0));
        String name = getStringValue(row.getCell(1));
        String description = getStringValue(row.getCell(2));
        ObjectKind objectKind = ObjectKind.getById(getIntValue(row.getCell(3)));
        String note = getStringValue(row.getCell(4));
        String oldName = getStringValue(row.getCell(5));
        return new ObjectType(id, name, description, objectKind, note, oldName, new HashSet<>(), new HashSet<>(), new ArrayList<>());
    }

    private void feedProperties(Set<ObjectType> objectTypes) {
        XSSFWorkbook objectTypeWb;
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertyFileName);
            if (inputStream == null) {
                throw new RuntimeException("Не удалось получить файл " + propertyFileName + " со свойствами.");
            }
            objectTypeWb = new XSSFWorkbook(inputStream);
            Sheet sheet = objectTypeWb.getSheetAt(0);
            Set<Property> properties = new HashSet<>();
            for (Row row : sheet) {
                if (row.getRowNum() != 0) {
                    properties.add(createProperty(row, objectTypes));
                }
            }
            for (Row row : sheet) {
                if (row.getRowNum() != 0) {
                    linkReferenceProperty(row, properties);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void linkReferenceProperty(Row row, Set<Property> properties) {
        int propertyId = getIntValue(row.getCell(0));
        if (propertyId < 1) {
            log.warn("Не удалось найти ID свойства");
            return;
        }

        int refPropertyId = getIntValue(row.getCell(9));
        if (refPropertyId < 1) {
            return;
        }
        Property referenceProperty = properties.stream().filter(property -> property.getId() == refPropertyId).findAny().orElse(null);
        Property property = properties.stream().filter(prop -> prop.getId() == propertyId).findAny().orElse(null);
        if (property == null) {
            log.warn("Не удалось найти ID свойства");
            return;
        }
        property.setReferenceProperty(referenceProperty);
    }

    private Property createProperty(Row row, Set<ObjectType> objectTypes) {
        int id = getIntValue(row.getCell(0));
        ObjectType objectType = getObjectTypeById(row.getCell(1), objectTypes);
        String name = getStringValue(row.getCell(2));
        String description = getStringValue(row.getCell(3));
        int propertySize = getIntValue(row.getCell(4));
        String propertyGroup = getStringValue(row.getCell(5));
        DataType dataType = DataType.getById(getIntValue(row.getCell(6)));
        String note = getStringValue(row.getCell(7));
        ObjectType referenceObjectType = getObjectTypeById(row.getCell(8), objectTypes);
        Property referenceProperty = null;
        boolean required = getBooleanValue(row.getCell(10));
        String valueFunction = getStringValue(row.getCell(11));
        boolean controlled = getBooleanValue(row.getCell(12));
        String oldName = getStringValue(row.getCell(13));
        boolean usingView = getBooleanValue(row.getCell(14));
        Property property = new Property(id,
                objectType,
                name,
                description,
                propertySize,
                propertyGroup,
                dataType,
                note,
                referenceObjectType,
                referenceProperty,
                required,
                valueFunction,
                controlled,
                oldName,
                usingView);

        if (objectType == null) {
            log.warn("Для свойства" + name + " (" + id + ") не найден объект");
        } else {
            objectType.getProperties().add(property);
        }

        if (referenceObjectType == null) {
            log.warn("Для ссылочного свойства" + name + " (" + id + ") не найден объект, на который он ссылается");
        } else {
            referenceObjectType.getProperties().add(property);
        }
        return property;
    }

    private ObjectType getObjectTypeById(Cell cell, Set<ObjectType> objectTypes) {
        final int objectTypeId = getIntValue(cell);
        return objectTypes.stream().filter(objectType -> objectType.getId() == objectTypeId).findAny().orElse(null);
    }

    private String getStringValue(Cell cell) {
        if (cell == null) {
            return "";
        }
        return cell.getStringCellValue();
    }

    private int getIntValue(Cell cell) {
        if (cell == null) {
            return -1;
        }
        return (int) cell.getNumericCellValue();
    }

    private boolean getBooleanValue(Cell cell) {
        return getIntValue(cell) == 1;
    }


}
