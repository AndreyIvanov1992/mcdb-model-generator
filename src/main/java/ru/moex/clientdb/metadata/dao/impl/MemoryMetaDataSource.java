package ru.moex.clientdb.metadata.dao.impl;

import lombok.AllArgsConstructor;
import org.springframework.util.StringUtils;
import ru.moex.clientdb.metadata.dao.MetaDataSource;
import ru.moex.clientdb.metadata.model.ObjectType;

import java.util.Set;


/**
 * Источник метаданных объектов ЕКБД в памяти
 */
@AllArgsConstructor
public class MemoryMetaDataSource implements MetaDataSource {

    private final Set<ObjectType> objectTypes;

    public ObjectType getObjectTypeByName(String name) {
        if(StringUtils.isEmpty(name)) {
            throw new IllegalArgumentException("name can't be empty");
        }
        return objectTypes.stream().filter(objectType -> name.equals(objectType.getName())).findAny().orElse(null);
    }

    public ObjectType getObjectTypeById(int id) {
        if(id < 1) {
            throw new IllegalArgumentException("id can't be empty");
        }
        return objectTypes.stream().filter(objectType -> objectType.getId() == id).findAny().orElse(null);
    }

    public Set<ObjectType> getObjectTypes() {
        return objectTypes;
    }
}
