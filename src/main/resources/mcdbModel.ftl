<#-- @ftlvariable name="" type="ru.moex.clientdb.metadata.service.modelgenerator.model.ObjectTypeClassModel" -->
package ${classPackage};

import ru.micex.clientdb.dao.model.common.${parentClass};
import ru.micex.clientdb.dao.model.common.Template;
import ru.micex.clientdb.dao.model.core.api.McdbModel;

import java.util.Arrays;
import java.util.List;

@McdbModel(${className}.OBJECT_TYPE_NAME)
public class ${className} extends ${parentClass} {

    public static final String OBJECT_TYPE_NAME = "${objectType.name}";

    <#list properties as property>
    <#if property.property.dataType != 'REFERENCE'>
    public ${property.type} ${property.getterMethodName}() {
        return ${property.parentGetterMethod}(FieldsNames.${property.constName});
    }

    public void ${property.setterMethodName}(${property.type} value) {
        ${property.parentSetterMethod}(FieldsNames.${property.constName}, value);
    }
    <#else>
    public ${property.type} ${property.getterMethodName}() {
        return getParentModel(FieldsNames.${property.constName}<#if property.allFieldsInCommonMethod>, ${property.type}.ALL_FIELDS</#if>);
    }

    public Optional&lt;${property.type}&gt; ${property.getterMethodName}_optional() {
        return getParentModelOptional(FieldsNames.${property.constName}<#if property.allFieldsInCommonMethod>, ${property.type}.ALL_FIELDS</#if>);
    }

    <#if !property.allFieldsInCommonMethod>
    public ${property.type} ${property.getterMethodName}(List&lt;String&gt; properties) {
        return getParentModel(FieldsNames.${property.constName}, properties);
    }

    public Optional&lt;${property.type}&gt; ${property.getterMethodName}_optional(List&lt;String&gt; properties) {
        return getParentModelOptional(FieldsNames.${property.constName}, properties>);
    }

    public ${property.type} ${property.getterMethodName}_allFields() {
        return getParentModel(FieldsNames.${property.constName}, ${property.type}.ALL_FIELDS);
    }

    public Optional&lt;${property.type}&gt; ${property.getterMethodName}_allFieldsOptional() {
        return getParentModelOptional(FieldsNames.${property.constName}, ${property.type}.ALL_FIELDS);
    }
    </#if>

    public void ${property.setterMethodName}(${property.type} value) {
        setParentModel(FieldsNames.${property.constName}, value);
    }
    </#if>
    </#list>

    <#list childGetters as childGetter>
    public List&lt;${childGetter.className}&gt; get${childGetter.baseNameMethod}() {
        return getChildModels(${childGetter.className}.class, ${childGetter.className}.FieldsNames.${childGetter.className}, new ArrayList<>());
    }

    public List&lt;${childGetter.className}&gt; get${childGetter.baseNameMethod}(List&lt;String&gt; properties) {
        return getChildModels(${childGetter.className}.class, ${childGetter.className}.FieldsNames.${childGetter.className}, properties);
    }

    public List&lt;${childGetter.className}&gt; get${childGetter.baseNameMethod}_allFields() {
        return getChildModels(${childGetter.className}.class, ${childGetter.className}.FieldsNames.${childGetter.className}, ${childGetter.className}.ALL_FIELDS);
    }

    public List&lt;${childGetter.className}&gt; get${childGetter.baseNameMethod}_withDeleted() {
        return getChildModels(${childGetter.className}.class, ${childGetter.className}.FieldsNames.${childGetter.className}, new ArrayList<>(), true);
    }

    public List&lt;${childGetter.className}&gt; get${childGetter.baseNameMethod}_withDeleted(List&lt;String&gt; properties) {
        return getChildModels(${childGetter.className}.class, ${childGetter.className}.FieldsNames.${childGetter.className}, properties, true);
    }

    public List&lt;${childGetter.className}&gt; get${childGetter.baseNameMethod}__withDeleteDallFields() {
        return getChildModels(${childGetter.className}.class, ${childGetter.className}.FieldsNames.${childGetter.className}, ${childGetter.className}.ALL_FIELDS, true);
    }
    </#list>

    public static final List&lt;String&gt ALL_FIELDS;

    static {
        ALL_FIELDS = new ArrayList<>();
        ALL_FIELDS.addAll(${parentClass}.ALL_FIELDS);
        <#list properties as property>
        ALL_FIELDS.add(FieldsNames.${property.constName});
        </#list>
    }

    public static class FieldsNames extends ${parentClass}.FieldsNames {

        <#list properties as property>

        public static final String ${property.constName} = "${property.property.getName()}";
        </#list>

    }
}
