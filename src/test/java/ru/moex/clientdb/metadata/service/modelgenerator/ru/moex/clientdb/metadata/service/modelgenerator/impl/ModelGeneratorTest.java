package ru.moex.clientdb.metadata.service.modelgenerator.ru.moex.clientdb.metadata.service.modelgenerator.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.moex.clientdb.metadata.service.modelgenerator.GeneratorSettings;
import ru.moex.clientdb.metadata.service.modelgenerator.ModelGenerator;
import ru.moex.clientdb.metadata.service.modelgenerator.ObjectTypeGenerationSettings;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ModelGeneratorTest {

    @Autowired
    private ModelGenerator modelGenerator;


    @Test
    public void test() {
        ObjectTypeGenerationSettings defaultSettings = ObjectTypeGenerationSettings.builder().
                basePackage("ru.micex.clientdb.dao.model").generateListsOfChild(true).build();

        GeneratorSettings generatorSettings = GeneratorSettings.builder().
                baseFolder("c:\\MoscowExchange\\models\\").
                defaultSettings(defaultSettings).objectTypesSettings(new HashMap<>()).
                objectTypes(new HashSet<>(Arrays.asList("instrument", "instrument_type", "decision", "placement", "v_sso_token_with_scope"))).
                build();
        modelGenerator.generateClassFiles(generatorSettings);
    }
}
