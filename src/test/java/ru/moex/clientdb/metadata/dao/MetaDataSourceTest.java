package ru.moex.clientdb.metadata.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.moex.clientdb.metadata.model.ObjectType;
import ru.moex.clientdb.metadata.model.Property;
import ru.moex.clientdb.metadata.model.DataType;
import ru.moex.clientdb.metadata.model.ObjectKind;

/**
 * Тесты источника данных метаинформации о типах объектов и проперти в ЕКБД
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MetaDataSourceTest {

    @Autowired
    private MetaDataSource metaDataSource;

    //Тесты пропертей

    private Property getProperty(int objectId, String propertyName) {
        return metaDataSource.getObjectTypeById(objectId).getProperty(propertyName);
    }

    @Test
    public void id_isInt() {
        assert getProperty(748, "id").getDataType() == DataType.INT;
    }

    @Test
    public void gateperfValue_isFloat() {
        assert getProperty(904, "gateperf_value").getDataType() == DataType.FLOAT;
    }

    @Test
    public void isin_isString() {
        assert getProperty(2, "isin").getDataType() == DataType.STRING;
    }

    @Test
    public void startShiftDate_isDate() {
        assert getProperty(750, "start_shift_date").getDataType() == DataType.DATE;
    }


    @Test
    public void lastEditTime_isDateTime() {
        assert getProperty(1136, "last_edit_time").getDataType() == DataType.DATETIME;
    }

    @Test
    public void ddlrReady_isBoolean() {
        assert getProperty(993, "ddlr_ready").getDataType() == DataType.BOOLEAN;
    }

    @Test
    public void binaryData_isBinary() {
        assert getProperty(63, "binary_data").getDataType() == DataType.BINARY;
    }

    @Test
    public void organizationId_isReference() {
        assert getProperty(2, "organization_id").getDataType() == DataType.REFERENCE;
    }

    @Test
    public void declarantFullNameId_is171() {
        assert getProperty(2, "declarant_full_name").getId() == 171;
    }

    @Test
    public void declarantFullNameDescription_isCorrect() {
        assert getProperty(2, "declarant_full_name").getDescription().equals("Полное наименование Заявителя");
    }

    @Test
    public void declarantFullNamePropertySize_is500() {
        assert getProperty(2, "declarant_full_name").getPropertySize() == 500;

    }

    @Test
    public void declarantFullNamePropertyGroup_isCorrect() {
        assert getProperty(2, "declarant_full_name").getPropertyGroup().equals("1. Общие свойства");
    }

    @Test
    public void declarantFullNameNote_isCorrect() {
        assert getProperty(2, "declarant_full_name").getNote().equals("Если не заполнено, считаем, что заявителем является Эмитент");
    }

    @Test
    public void instrumentOrganizationIdReferenceObjectType_isOrganization() {
        ObjectType organization = metaDataSource.getObjectTypeByName("organization");
        assert getProperty(200, "instrument_organization_id").getReferenceObjectType().equals(organization);
    }

    @Test
    public void instrumentOrganizationIdReferenceObjectProperty_isShortName() {
        Property shortName = metaDataSource.getObjectTypeByName("organization").getProperty("short_name");
        assert getProperty(200, "instrument_organization_id").getReferenceProperty().equals(shortName);
    }

    @Test
    public void declarantFullNameRequired_isFalse() {
        assert !getProperty(2, "declarant_full_name").isRequired();
    }

    @Test
    public void exceptionPlacementIdRequired_isTrue() {
        assert getProperty(899, "exception_placement_id").isRequired();
    }

    @Test
    public void instrumentOrganizationIdValueFunction_isIcdbFnVirtualProperty1384() {
        assert getProperty(200, "instrument_organization_id").getValueFunction().equals("ICDB.FN_VIRTUAL_PROPERTY_1384");
    }

    @Test
    public void declarantFullNameControled_isFalse() {
        assert !getProperty(2, "declarant_full_name").isControlled();
    }

    @Test
    public void exceptionPlacementIdControled_isTrue() {
        assert getProperty(899, "exception_placement_id").isControlled();
    }

    @Test
    public void declarantFullNameOldName_declarantFullName() {
        assert getProperty(2, "declarant_full_name").getOldName().equals("declarant_full_name");
    }

    @Test
    public void declarantFullNameUsingView_isFalse() {
        assert !getProperty(2, "declarant_full_name").isUsingView();
    }

    @Test
    public void isIncludedUsingView_isTrue() {
        assert getProperty(1023, "is_included").isUsingView();
    }

    //Тесты типов объектов

    @Test
    public void riskLevelId_is238() {
        assert metaDataSource.getObjectTypeByName("risk_level").getId() == 238;
    }

    @Test
    public void object238_isRiskLevel() {
        assert metaDataSource.getObjectTypeById(238).getName().equals("risk_level");
    }

    @Test
    public void riskLevelDescription_isCorrect() {
        assert metaDataSource.getObjectTypeByName("risk_level").getDescription().equals("Уровень риска клиента");
    }

    @Test
    public void personalManager_isDictionary() {
        assert metaDataSource.getObjectTypeByName("personal_manager").getObjectKind() == ObjectKind.INFORMATION_OBJECT;
    }

    @Test
    public void decision_isInformationObject() {
        assert metaDataSource.getObjectTypeByName("decision").getObjectKind() == ObjectKind.INFORMATION_OBJECT;
    }

    @Test
    public void riskLevelDataType_isInformationObject() {
        assert metaDataSource.getObjectTypeByName("risk_level").getObjectKind() == ObjectKind.DICTIONARY;
    }

    @Test
    public void documentsOfOrganizations_isView() {
        assert metaDataSource.getObjectTypeByName("documents_of_organizations").getObjectKind() == ObjectKind.VIEW;
    }

    @Test
    public void riskLevelNote_isCorrect() {
        assert metaDataSource.getObjectTypeByName("risk_level").getNote().equals("Присваиается для каждого клиента в соответствии с критериями, установленными Правилами ПОД/ФТ ЗАО \"ФБ ММВБ\", утвержденными Приказом Генерального директора от 05.03.2011 №2-од");
    }

    @Test
    public void riskLevelOldName_isRiskLevel() {
        assert metaDataSource.getObjectTypeByName("risk_level").getOldName().equals("risk_level");
    }
}